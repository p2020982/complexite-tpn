#include "tas.hpp"

#include <vector>
#include <algorithm>
#include <random>
#include <iostream>
#include <cassert>

#define TEST_LIENS
#define TEST_NIVEAU
#define TEST_VALIDITE
#define TEST_MIN_MAX
#define TEST_INSERTION
#define TEST_MEILLEUR_ENFANT
#define TEST_ENTERRER

std::vector<int> test_input[] = {
  { 59, 24, 47, 9, 17, 31, 28, 66, 26, 95, 89, 16, 48, 99, 51, 73, 38, 92, 22, 32 },
  { 39, 56, 30, 9, 50, 80, 67, 23, 38, 92, 48, 77, 50, 82, 0, 86, 98, 31, 41, 95 }
} ;

std::vector<int> test_tas[] = { 
  {                                  9, 
/*                     /                        \                */
                      95,                       99, 
/*            /                \           /         \           */
             22,               17,        16,        28, 
/*       /        \          /    \     /     \    /    \        */
        73,       92,       66,   89,  31,   47,  48,   51, 
/*     /  \      /  \      /                                     */
      59, 38,   26, 24,   32 
  },
  {                                  0, 
/*                    /                         \                */
                     98,                        82, 
/*          /                \             /         \           */
           23,               48,          39,         9, 
/*      /       \          /    \      /     \      /   \        */
      92,       41,       95,   50,   77,    50,   80,  67, 
/*   /  \      /  \      /                                       */
    30, 86,   31, 38,   56 
  }
} ;


std::ostream& operator<<(std::ostream& out, const std::vector<int>& tab) {
  out << "{" ;
  bool first = true ;
  for(int i : tab) {
    if(!first) {
      out << ", " ;
    } else {
      out << " " ;
      first = false ;
    }
    out << i ;
  }
  out << " }" ;
  return out ;
}

std::ostream& operator<<(std::ostream& out, const Tas& tas) {
  out << tas.tas ;
  return out ;
}

int main() {

#ifdef TEST_LIENS
  {
    std::cout << "=================== test liens ===================" << std::endl ;

    Tas tas ;

    std::cout << "attendu : 2" << std::endl ;
    std::cout << tas.parent(5) << std::endl ;
    std::cout << "attendu : 5" << std::endl ;
    std::cout << tas.gauche(2) << std::endl ;
    std::cout << "attendu : 18" << std::endl ;
    std::cout << tas.droite(8) << std::endl ;
  }
#endif

#ifdef TEST_NIVEAU
  {
    std::cout << "=================== test niveau ===================" << std::endl ;

    Tas tas ;

    std::cout << "attendu : 2" << std::endl ;
    std::cout << tas.niveau(5) << std::endl ;
    std::cout << "attendu : 4" << std::endl ;
    std::cout << tas.niveau(18) << std::endl ;
    std::cout << "attendu : 7" << std::endl ;
    std::cout << tas.niveau(220) << std::endl ;
  }
#endif

#ifdef TEST_VALIDITE
  {
    std::cout << "=================== test validite ===================" << std::endl ;

    Tas tas ;

    tas.tas = test_tas[0] ;
    std::cout << "attendu : valide" << std::endl ;
    try {
      tas.structure_valide() ;
      std::cout << "valide" << std::endl ;
    } catch(std::exception& e) {
      std::cout << e.what() << std::endl ;
    }

    tas.tas = test_tas[1] ;
    std::cout << "attendu : valide" << std::endl ;
    try {
      tas.structure_valide() ;
      std::cout << "valide" << std::endl ;
    } catch(std::exception& e) {
      std::cout << e.what() << std::endl ;
    }

    tas.tas = test_tas[0] ;
    tas.tas[8] = 3 ;
    std::cout << "attendu : le parent 22 de 3 est incorrect" << std::endl ;
    try {
      tas.structure_valide() ;
      std::cout << "valide" << std::endl ;
    } catch(std::exception& e) {
      std::cout << e.what() << std::endl ;
    }

    tas.tas = test_tas[1] ;
    tas.tas[15] = 16 ;
    std::cout << "attendu : le grand parent 23 de 16 est incorrect" << std::endl ;
    try {
      tas.structure_valide() ;
      std::cout << "valide" << std::endl ;
    } catch(std::exception& e) {
      std::cout << e.what() << std::endl ;
    }


  }
#endif

#ifdef TEST_MIN_MAX
  {
    std::cout << "=================== test min max ===================" << std::endl ;

    Tas tas ;

    tas.tas = test_tas[0] ;
    std::cout << "attendu : 9" << std::endl ;
    std::cout << tas.min() << std::endl ;
    std::cout << "attendu : 99" << std::endl ;
    std::cout << tas.max() << std::endl ;

    tas.tas = test_tas[1] ;
    std::cout << "attendu : 0" << std::endl ;
    std::cout << tas.min() << std::endl ;
    std::cout << "attendu : 98" << std::endl ;
    std::cout << tas.max() << std::endl ;
  }
#endif

#ifdef TEST_INSERTION
  {
    std::cout << "=================== test insertion ===================" << std::endl ;

    Tas tas ;
    for(int i : test_input[0]) {
      tas.inserer(i) ;
    }

    std::cout << "attendu : " << test_tas[0] << std::endl ;
    std::cout << "          " << tas << std::endl ;
    tas.structure_valide() ;

    tas.tas.clear() ;
    for(int i : test_input[1]) {
      tas.inserer(i) ;
    }

    std::cout << "attendu : " << test_tas[1] << std::endl ;
    std::cout << "          " << tas << std::endl ;
    tas.structure_valide() ;
  }
#endif

#ifdef TEST_MEILLEUR_ENFANT
  {
    std::cout << "=================== test meilleur enfant ===================" << std::endl ;

    Tas tas ;
    int enfant ;
 
    tas.tas = test_tas[0] ;

    std::cout << "pour : " << tas.tas[3] << std::endl ;
    std::cout << "attendu : 18 (valeur 24)" << std::endl ;
    enfant = tas.meilleur_enfant(false, 3) ;
    std::cout << enfant << " (valeur " << tas.tas[enfant] << " )" << std::endl ;

    std::cout << "pour : " << tas.tas[8] << std::endl ;
    std::cout << "attendu : 17 (valeur 26)" << std::endl ;
    enfant = tas.meilleur_enfant(true, 8) ;
    std::cout << enfant << " (valeur " << tas.tas[enfant] << " )" << std::endl ;

    tas.tas = {
            0, 
/*       /     \        */
        57,    34, 
/*     /  \             */
      54, 38
    } ;

    std::cout << "pour : " << tas.tas[0] << std::endl ;
    std::cout << "attendu : 2 (valeur 34)" << std::endl ;
    enfant = tas.meilleur_enfant(false, 0) ;
    std::cout << enfant << " (valeur " << tas.tas[enfant] << " )" << std::endl ;

  }
#endif

#ifdef TEST_ENTERRER
  {
    std::cout << "=================== test enterrer ===================" << std::endl ;

    Tas tas ;
    tas.tas = test_tas[0] ;

    tas.tas[0] = 45 ;
    std::cout << "depuis  : " << tas.tas << std::endl ;

    tas.enterrer(0) ;
    std::cout << "attendu : " << "{ 16, 95, 99, 22, 17, 31, 28, 73, 92, 66, 89, 45, 47, 48, 51, 59, 38, 26, 24, 32 }" << std::endl ;
    std::cout << "          " << tas << std::endl ;
    tas.structure_valide() ;


    tas.tas = test_tas[1] ;

    tas.tas[0] = 90 ;
    std::cout << "depuis  : " << tas.tas << std::endl ;

    tas.enterrer(0) ;
    std::cout << "attendu : " << "{ 9, 98, 90, 23, 48, 39, 67, 92, 41, 95, 50, 77, 50, 80, 82, 30, 86, 31, 38, 56 }" << std::endl ;
    std::cout << "          " << tas << std::endl ;
    //tas.structure_valide() ;
  }
#endif

    Tas tas;
    tas.tas = test_tas[0];
    std::cout << tas.tas.size() << std::endl;
    while(tas.tas.size()>0) {
        std::cout << tas.pop_min() << " ";
    }
    Tas tas2;
    tas2.tas = test_tas[1];
    std::cout << std::endl << tas2.tas.size() << std::endl;
    while(tas2.tas.size()>0) {
        std::cout << tas2.pop_max() << " ";
    }


  return 0 ;
}
