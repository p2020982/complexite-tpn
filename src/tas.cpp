#include "tas.hpp"

#include <exception>
#include <iostream>
#include <sstream>

int Tas::parent(int n) {
  //a remplacer
  return (n-1)/2 ;
}

int Tas::gauche(int n) {
  //a remplacer
  return 2*n+1 ;
}

int Tas::droite(int n) {
  //a remplacer
  return 2*n+2 ;
}

int Tas::niveau(int n) {
  //a remplacer
  if(n==0) {
      return 0;
  }
  return niveau(parent(n)) + 1 ;
}

int Tas::min() {
  return tas[0] ;
}

int Tas::max() {
  //a remplacer
  return std::max(tas[1], tas[2]) ;
}

bool Tas::parent_valide(bool niveau_max, int valeur, int valeur_parent) {
  //a
  if(niveau_max) {
      return valeur >= valeur_parent;
  }
  return valeur <= valeur_parent;
}

bool Tas::grand_parent_valide(bool niveau_max, int valeur, int valeur_grand_parent) {
  //a remplacer
  if(niveau_max) {
      return valeur <= valeur_grand_parent;
  }
  return valeur >= valeur_grand_parent;
}

void Tas::structure_valide() {
  //taille du tas
  int size = tas.size() ;

  //valider tous les éléments du tas
  for(int i = 0; i < size; ++i) {
    //type de niveau de l'élément
    bool max_lvl = niveau(i) % 2 ;

    //validité du parent
    if( i > 0 && !parent_valide(max_lvl, tas[i], tas[parent(i)])) {
      std::stringstream ss ;
      ss << "le parent " << tas[parent(i)] << " de " << tas[i] << " n'est pas correct" ;
      throw std::domain_error(ss.str()) ;
    }

    //validité du grand parent
    if( i > 2 && !grand_parent_valide(max_lvl, tas[i], tas[parent(parent(i))])) {
      std::stringstream ss ;
      ss << "le grand parent " << tas[parent(parent(i))] << " de " << tas[i] << " n'est pas correct" ;
      throw std::domain_error(ss.str()) ;
    }
  }
}

void Tas::remonter(int n) {
    if(niveau(n)%2==0) {
        if(n!=0) {
            if(tas[n]<=tas[parent(n)]) {
                int cur = n;
                int gp = parent(parent(n));
                while(cur != 0 && tas[cur] < tas[gp]) {
                    int tmp = tas[cur];
                    tas[cur] = tas[gp];
                    tas[gp] = tmp;
                    cur = gp;
                    gp = parent(parent(cur));
                }
            } else {
                int par = parent(n);
                int tmp2 = tas[n];
                tas[n] = tas[par];
                tas[par] = tmp2;
                int cur = par;
                if(niveau(cur)!=1) {
                    int gp = parent(parent(cur));
                    while(niveau(cur)!=1 && tas[cur] > tas[gp]) {
                        int tmp = tas[cur];
                        tas[cur] = tas[gp];
                        tas[gp] = tmp;
                        cur = gp;
                        gp = parent(parent(cur));
                    }
                }
            }
        }
    } else { //impairs
        if(tas[n]>=tas[parent(n)]) {
            int cur = n;
            if(niveau(cur)!=1) {
                int gp = parent(parent(cur));
                while(niveau(cur)!=1 && tas[cur] > tas[gp]) {
                    int tmp = tas[cur];
                    tas[cur] = tas[gp];
                    tas[gp] = tmp;
                    cur = gp;
                    gp = parent(parent(cur));
                }
            }
        } else {
            int par = parent(n);
            int tmp2 = tas[n];
            tas[n] = tas[par];
            tas[par] = tmp2;
            int cur = par;
            if(cur!=0) {
                int gp = parent(parent(cur));
                while(cur != 0 && tas[cur] < tas[gp]) {
                    int tmp = tas[cur];
                    tas[cur] = tas[gp];
                    tas[gp] = tmp;
                    cur = gp;
                    gp = parent(parent(cur));
                }
            }
        }
    }
}

void Tas::inserer(int val) {
  tas.push_back(val);
  remonter(tas.size()-1);
}

int Tas::meilleur_enfant(bool niveau_max, int n) {
    if(!niveau_max) {
        int g = gauche(n);
        int d = droite(n);
        if(tas.size() <= gauche(g)) {
            int min = std::min(tas[g], tas[d]);
            if(min==tas[g])
                return g;
            return d;
        } else {
            if(tas.size() <= gauche(d)) {
                int min = std::min(tas[gauche(g)], tas[droite(g)]);
                min = std::min(min, tas[d]);
                if(min==tas[d])
                    return d;
                if(min==tas[gauche(g)])
                    return gauche(g);
                return gauche(d);
            } else {
                int min1=std::min(tas[gauche(g)], tas[droite(g)]);
                int min2=std::min(tas[gauche(d)], tas[droite(d)]);
                int min = std::min(min1, min2);
                if(min==tas[gauche(g)])
                    return gauche(g);
                if(min==tas[droite(g)])
                    return droite(g);
                if(min==tas[gauche(d)])
                    return gauche(d);
                return droite(d);
            }
        }
    }
    int g = gauche(n);
    int d = droite(n);
    if(tas.size() <= gauche(g)) {
        int max = std::max(tas[g], tas[d]);
        if(max==tas[g])
            return g;
        return d;
    } else {
        if(tas.size() <= gauche(d)) {
            int max = std::max(tas[gauche(g)], tas[droite(g)]);
            max = std::max(max, tas[d]);
            if(max==tas[d])
                return d;
            if(max==tas[gauche(g)])
                return gauche(g);
            return gauche(d);
        } else {
            int max1=std::max(tas[gauche(g)], tas[droite(g)]);
            int max2=std::max(tas[gauche(d)], tas[droite(d)]);
            int max = std::max(max1, max2);
            if(max==tas[gauche(g)])
                return gauche(g);
            if(max==tas[droite(g)])
                return droite(g);
            if(max==tas[gauche(d)])
                return gauche(d);
            return droite(d);
        }
    }
  return 0 ;
}

void Tas::enterrer(int n) {
    if(gauche(n)<tas.size() && niveau(n) != niveau(tas.size()-1)) {
        bool niveau_max = false;
        if(niveau(n)%2==1)
            niveau_max=true;
        if(niveau_max) {
            int me = meilleur_enfant(niveau_max, n);
            if(tas[me]<=tas[n]) {
                return;
            }
            int tmp = tas[n];
            tas[n] = tas[me];
            tas[me] = tmp;
            enterrer(me);
        } else {
            int me = meilleur_enfant(niveau_max, n);
            if(tas[me]>=tas[n]) {
                return;
            }
            int tmp = tas[n];
            tas[n] = tas[me];
            tas[me] = tmp;
            enterrer(me);
        }
    }
}

int Tas::pop_min() {
    int ret = tas[0];
    tas[0] = tas[tas.size()-1];
    tas.pop_back();
    enterrer(0);
    return ret;
}

int Tas::pop_max() {
    int max = std::max(tas[1], tas[2]);
    int i = max == tas[1] ? 1 : 2;
    tas[i] = tas[tas.size()-1];
    tas.pop_back();
    enterrer(i);
    return max;
}
