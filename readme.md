# Tas min max

Le but de ce TP est d'implémenter la structure de *tas min max*. Il s'agit d'une
variante de tas qui permet de fournir à la fois le minimum et le maximum d'un
ensemble de valeurs. Par rapport à un tas classique dans le quel chaque élément
est plus petit que toute sa descendance (pour un tas fournissant le minimum), le
tas min max alterne des niveaux minimaux et maximaux : sur les niveaux pairs,
les éléments sont plus petits que toute leur descendance. Sur les niveaux
impairs, les éléments sont plus grands que toute leur descendance.

## Mise en place

Clonez le sujet du TP. Vous pouvez compiler soit via le `Makefile` fourni dans
le dossier `src`, soit via `cmake`. Si ça ne fonctionne pas pour vous, vous pouvez
dans le dossier `src` et compiler via 

```bash
g++ *.cpp -o test_tas
```

La compilation génère un exécutable `test_tas` qu'il faudra exécuter au fur et à
mesure de ce TP. Cet exécutable ne fait pour l'instant rien, il faudra
décommenter des tests au fur et à mesure de votre avancée.

## Conditions de rendu

Le TP est à rendre sur TOMUSS à 17h15. En cas de tiers temps, 18h. Créez une
archive du dossier de votre TP, et pour économiser de la place, pensez à
nettoyer les résultats de compilation. Sous linux, en ligne de commande, en
supposant que vous ayez cloné le sujet sans en modifier le nom, une archive peut
être créée via

```bash
tar czvf tp_nom_prenom.tar.gz tas-min-max-etu
```

## Liens de parenté

Dans cette partie, vous mettrez en place les petites fonctions utiles pour les
tas et pour la suite. Un tas min max est un arbre stocké dans un tableau de la
même manière qu'un tas classique :

* l'enfant gauche d'un élément à l'indice $i$ est à l'indice $2i+1$ ;
* l'enfant droit d'un élément à l'indice $i$ est à l'indice $2i+2$ ;
* le parent d'un élément à l'indice $i$ est à l'indice $\frac{i-1}{2}$ arrondi
  à l'entier inférieur.

### Votre travail

Dans le fichier `tas.cpp` complétez les fonctions `gauche`, `droite` et
`parent`. Une fois réalisées vous pouvez activer le test correspondant en
décommentant au début du fichier `test_tas.cpp` la ligne 

```cpp
#define TEST_LIENS
```

## Validité d'un tas min max

Dans un tas min max, les niveaux sont alternés : 
* sur un niveau pair (0 inclus), chaque élément est le minimum de sa descendance
* sur un niveau impair, chaque élément est le maximum de sa descendance

Par exemple, l'arbre suivant correspond à un tas min max.

![tas min max](img/tas_min_max.svg)

### Niveau d'un élément

Pour pouvoir savoir quelles sont les contraintes qu'un élément doit respecter,
il est nécessaire de savoir s'il se trouve sur un niveau pair ou impair. Nous
allons donc commencer par déterminer sur quel niveau se situe un élément. Une
méthode simple consiste à dire que si un élément a l'indice 0, il est sur le
niveau 0. Sinon, l'indice de son niveau est un de plus que le niveau de son
parent.

#### Votre travail

Implémentez la méthode `niveau` dans le fichier `tas.cpp`. Une fois cette
méthode disponible, activez le test correspondant en décommentant dans
`test_tas.cpp` la ligne 

```cpp
#define TEST_NIVEAU
```

### Validité des parents

Une fois le niveau correctement implémenté, vous pouvez vous attaquer à la
vérification de la validité du tas. Pour respecter les contraintes d'ordre 
entre les parents et les enfants, il est possible de tester pour chaque élément
du tas les contraintes suivantes :

* sur un niveau pair, un élément est plus petit que son parent, et plus grand
  que son grand parent
* sur un niveau impair, un élément est plus grand que son parent et plus petit
  que son grand parent

#### Votre travail

Implémentez les méthodes `parent_valide` et `grand_parent_valide` qui prennent
en paramètre :

* un booléen indiquant si l'élément est sur un niveau maximal ou non
* la valeur de l'élément
* la valeur du (grand) parent

Ces fonctions vérifient que l'ordre est respecté. Lorsque le parent ou le grand
parent n'existe pas, il est valide. Les éléments peuvent également être égaux à
leurs parents et grands parents. Une fois ces fonctions implémentées, vous
pouvez décommenter dans le fichier `test_tas.cpp` la ligne

```cpp
#define TEST_VALIDITE
```

## Consultation du minimum et du maximum

Dans un tas min max, le niveau 0 est un niveau minimum, la racine du tas est
donc le minimum du tas. Le niveau 1 est un niveau maximum. Chacun des deux
éléments de ce niveau est à la fois plus grand que la racine, et plus grand que
toute sa descendance. Le maximum est donc l'un des deux éléments du niveau 1.

### Votre travail

Implémentez les méthodes `min` et `max` du tas. Une fois disponibles, vous
pouvez décommenter dans le fichier `test_tas.cpp` la ligne

```cpp
#define TEST_MIN_MAX
```

## Insertion

Nous rentrons maintenant dans le vif du sujet : ajouter des valeurs dans le tas.
L'ajout commence comme dans un tas classique : l'élément ajouté est placé à la
fin du tableau du tas. Ensuite il est remonté le long du chemin vers la racine
pour se placer correctement par rapport à ses ancêtres.

La première choses à vérifier est la validité du parent de la valeur insérée. Si
la valeur est insérée sur un niveau minimum, son parent est sur un niveau
maximum et elle doit être plus petite que son parent. Si c'est le cas, elle est
alors plus petite que tous ses ancêtres situés sur des niveaux maximum. Elle
peut donc se placer sur n'importe quel niveau minimum le long du chemin vers la
racine sans poser de problème par rapport aux éléments de ce chemin sur des
niveaux maximum. On peut donc réaliser le même procédé que pour faire remonter
les valeurs d'un tas binaire classique, mais en se comparant avec les grands
parents et en ne se préoccupant que des niveaux minimum.

![initialisation insertion, cas inf](img/insertion_init_inf.svg)

Si par contre l'élément inséré sur un niveau minimum est plus grand que son
parent, par définition de l'arbre ce parent est plus grand que tous ses ancêtres
situés sur des niveaux minimum sur le chemin vers la racine. L'élément peut donc
être déposé sur n'importe quel niveau maximum sans se préoccuper des éléments
sur les niveaux minimum. On commence donc par l'échanger avec son parent pour le
placer sur le niveau maximum le plus bas, puis on le fait remonter à la manière
d'un tas binaire classique, mais encore une fois en se comparant avec les grands
parents.

![initialisation insertion, cas sup](img/insertion_init_sup.svg)

En résumé :

* si le parent de l'élément inséré n'est pas valide, échanger l'élément avec son
  parent ;
* tant que le grand parent de l'élément inséré n'est pas valide, échanger l'élément
  avec son grand parent.

Il reste à faire attention aux cas où l'élément est remonté en haut de l'arbre : 
s'il est à l'indice 0 c'est la racine qui n'a ni parent ni grand parent, s'il
est à l'indice 1 ou 2 il n'a pas de grand parent.

### Votre travail

Implémentez l'insertion dans le tas. Cette insertion est découpée sur deux
méthodes : la méthode `inserer` qui ajoute l'élément au tableau du tas et
appelle la méthode `remonter` qui se charge de le faire remonter sur le chemin
de ses ancêtres vers la racine. Une fois ces deux méthodes disponibles, vous
pouvez décommenter dans le fichier `test_tas.cpp` la ligne

```cpp
#define TEST_INSERTION
```

## Suppression

Pour terminer sur les fonctionnalités du tas, il reste à en extraire le maximum
ou le minimum. De même que pour l'insertion, cette extraction commence comme
l'extraction sur un tas binaire classique : la valeur extraite est remplacée par
la dernière valeur du tableau du tas, puis le tableau est réduit d'une case.
Ensuite cette valeur est redescendue dans l'arbre à une position qui permet de
respecter les contraintes.

### Choix du descendant pour descendre un élément

Pour pouvoir descendre un élément, il faut d'abord identifier par qui le
remplacer. Nous raisonnerons ici sur un élément sur un niveau minimum, le cas
maximum est symétrique. Si l'élément est sur un niveau minimum, il faut que
l'élément qui le remplacera soit plus petit que toute sa descendance. Cet
élément est a priori l'un des petits enfants de l'élément, mais lorsqu'il manque
des petits enfants, il peut également s'agir d'un des enfants. On peut
simplifier les choses en disant que pour un élément sur un niveau minimum il
faut chercher l'élément minimal parmi les enfants et petits enfants.

![meilleur enfant](img/descendant.svg)

#### Votre travail

Implémentez la méthode `meilleur_enfant` qui prend en paramètres le type de
niveau sur lequel se trouve l'élément et l'indice d'un élément et retourne
l'indice du plus petit (sur un niveau minimum) ou du plus grand (sur un niveau
maximum) élément parmi les enfants et petits enfants de l'élément. Vous pouvez
tester cette fonctionnalité en décommentant dans le fichier `test_tas.cpp` la
ligne

```cpp
#define TEST_MEILLEUR_ENFANT
```

### Descente d'un élément

Une fois le meilleur enfant déterminé, il faut déterminer si l'élément à
descendre est un grand parent valide pour cet enfant. Si ce n'est pas le cas il
faut les échanger. Dans le cas où cet échange a lieu avec un petit enfant, il
est possible que l'élément à redescendre se retrouve sous un parent invalide.

![descente](img/descente.svg)

Dans ce cas, l'élément à descendre doit être échangé avec son parent, et c'est
ce parent qui continuera à descendre à sa place. Lorsque l'élément à descendre
n'a plus d'enfants, ou lorsqu'il est un grand parent valide pour son meilleur
enfant, la descente s'arrête. Pour récapituler :

* si l'élément à descendre n'a pas d'enfants, la descente est terminée
* déterminer le meilleur enfant de l'élément à descendre
* si l'élément à descendre est un grand parent valide pour ce meilleur enfant,
  la descente est terminée
* sinon échanger l'élément à descendre avec son meilleur enfant
* si l'élément à descendre a un parent invalide, l'échanger avec son parent qui
  descendra à sa place
* boucler

#### Votre travail

Implémentez la méthode `enterrer` qui fait descendre un élément dans le tas. Une
fois cette méthode disponible, décommentez dans le fichier `test_tas.cpp` la
ligne

```cpp
#define TEST_ENTERRER
```

### Extraction du min et du max

Avec le mécanisme de descente des éléments, vous pouvez désormais implémentez
les fonctions d'extraction de minimum et de maximum.

#### Votre travail

Implémentez les fonctions `pop_min` et `pop_max` qui réalisent ces extractions.
Modifiez ensuite le fichier `test_tas.cpp` pour ajouter un test qui crée un tas
puis en extrait toutes les valeurs et s'assure qu'elles sont triées. Avec
`pop_min` elles devraient sortir dans l'ordre croissant, avec `pop_max` dans
l'ordre décroissant.
